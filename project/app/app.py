from flask import Flask, request, render_template, json, redirect, url_for, session
from blueprint_query.route import blueprint_query
from blueprint_auth.route import blueprint_auth
from blueprint_account.route import blueprint_account
from blueprint_report.route import blueprint_report
from blueprint_booking.route import blueprint_booking
from access import login_required, group_required, external_required
from typing import List, Callable

app = Flask(__name__)
app.secret_key = 'Key'

app.register_blueprint(blueprint_auth, url_prefix='/auth')
app.register_blueprint(blueprint_query, url_prefix='/query')
app.register_blueprint(blueprint_account, url_prefix='/account')
app.register_blueprint(blueprint_booking, url_prefix='/booking')
app.register_blueprint(blueprint_report, url_prefix='/report')

with open('config/dbconfig.json', 'r') as file:
    dbconfig = json.load(file)
app.config['dbconfig'] = dbconfig

with open('config/access.json', 'r') as file:
    access_config = json.load(file)
app.config['access_config'] = access_config

with open('config/report_list.json', 'r') as file:
    report_list = json.load(file)
app.config['report_list'] = report_list

with open('config/report_url.json', 'r') as file:
    report_url = json.load(file)
app.config['report_url'] = report_url


@app.route('/')
def start_func():
    if 'user_id' in session:
        return render_template('start_page_auth.html')
    else:
        return render_template('start_page_no_auth.html')


@app.route('/menu')
@login_required
def menu_choice():
    group = session.get('user_group')
    if group == 'director':
        return render_template('menu/internal_user_director.html')
    elif group == 'analyst':
        return render_template('menu/internal_user_analyst.html')
    return render_template('menu/external_user.html')


@app.route('/exit')
def exit_func():
    session.clear()
    return render_template('exit_request.html')


def add_blueprint_access_handler(_app: Flask, blueprint_names: List[str], handler: Callable) -> Flask:
    for view_func_name, view_func in _app.view_functions.items():
        view_func_parts = view_func_name.split('.')
        if len(view_func_parts) > 1:
            view_blueprint = view_func_parts[0]
            if view_blueprint in blueprint_names:
                view_func = handler(view_func)
                _app.view_functions[view_func_name] = view_func
    return _app


if __name__ == '__main__':
    app = add_blueprint_access_handler(app, ['bp_account', 'bp_booking'], external_required)
    app = add_blueprint_access_handler(app, ['bp_query', 'bp_report'], group_required)
    app = add_blueprint_access_handler(app, ['bp_query', 'bp_report', 'bp_account', 'bp_booking'], login_required)
    app.run(host='0.0.0.0', port=5001)
