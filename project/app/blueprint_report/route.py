from flask import Blueprint, render_template, request, current_app, session, redirect, url_for
from sql_provider import SQLProvider
from db_work import call_proc, select_dict
import os

blueprint_report = Blueprint('bp_report', __name__, template_folder='templates')

provider = SQLProvider(os.path.join(os.path.dirname(__file__), 'sql'))


@blueprint_report.route('/', methods=['GET', 'POST'])
def start_report():
    rep_list = current_app.config['report_list']['reports']
    rep_url_dict = current_app.config['report_url']
    if request.method == 'GET':
        group = session.get('user_group')
        if group == 'director':
            return render_template('menu/report_director.html', report_list=rep_list)
        return render_template('menu/report_analyst.html', report_list=rep_list)
    else:
        rep_id = request.form.get('rep_id')
        if request.form.get('create_rep'):
            url = rep_url_dict[rep_id]['create_report']
        else:
            url = rep_url_dict[rep_id]['view_report']
        return redirect(url_for(url))


@blueprint_report.route('/create/bonus_report', methods=['GET', 'POST'])
def create_bonus_report():
    if request.method == 'GET':
        return render_template('reports/bonus_report_form.html')
    else:
        date_str = request.form.get('month_num')
        year, month = date_str.split('-')
        _sql = provider.get('bonus_report_check.sql', year_input=year, month_input=month)
        result = select_dict(current_app.config['dbconfig'], _sql)
        if result[0]['COUNT(*)'] == 0:
            if call_proc(current_app.config['dbconfig'], 'bonus_report', month, year):
                return render_template('report_created.html')
        else:
            return render_template('reports/bonus_report_form.html', error='Отчет уже создан в системе')


@blueprint_report.route('/create/ticket_report', methods=['GET', 'POST'])
def create_ticket_report():
    if request.method == 'GET':
        return render_template('reports/ticket_report_form.html')
    else:
        date_str = request.form.get('month_num')
        year, month = date_str.split('-')
        _sql = provider.get('ticket_report_check.sql', year_input=year, month_input=month)
        result = select_dict(current_app.config['dbconfig'], _sql)
        if result[0]['COUNT(*)'] == 0:
            if call_proc(current_app.config['dbconfig'], 'ticket_report', month, year):
                return render_template('report_created.html')
        else:
            return render_template('reports/ticket_report_form.html', error='Отчет уже создан в системе')


@blueprint_report.route('/view/bonus_report', methods=['GET', 'POST'])
def view_bonus_report():
    if request.method == 'GET':
        return render_template('reports/bonus_report_form.html')
    else:
        date_str = request.form.get('month_num')
        year, month = date_str.split('-')
        _sql = provider.get('bonus_report_view.sql', year_input=year, month_input=month)
        result = select_dict(current_app.config['dbconfig'], _sql)
        if result:
            amount = result[0]['brep_amount']
            return render_template('reports/bonus_report_view.html', month=month, year=year, amount=amount)
        else:
            return render_template('reports/bonus_report_form.html', error='За указанный период отчет отсутствует')


@blueprint_report.route('/view/ticket_report', methods=['GET', 'POST'])
def view_ticket_report():
    if request.method == 'GET':
        return render_template('reports/ticket_report_form.html')
    else:
        date_str = request.form.get('month_num')
        year, month = date_str.split('-')
        _sql = provider.get('ticket_report_view.sql', year_input=year, month_input=month)
        result = select_dict(current_app.config['dbconfig'], _sql)
        if result:
            amount = result[0]['trep_amount']
            return render_template('reports/ticket_report_view.html', month=month, year=year, amount=amount)
        else:
            return render_template('reports/ticket_report_form.html', error='За указанный период отчет отсутствует')
