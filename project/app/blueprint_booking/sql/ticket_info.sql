SELECT f_id AS id,
       TIME_FORMAT(TIME(f_dep_date), '%H:%i') AS dep_time,
       TIME_FORMAT(TIME(f_arr_date), '%H:%i') AS arr_time,
       TIME_FORMAT(TIMEDIFF(CONVERT_TZ(f_arr_date, f_arr_time_zone, '+00:00'), CONVERT_TZ(f_dep_date, f_dep_time_zone, '+00:00')), '%Hh %imin') AS period,
       DATE_FORMAT(DATE(f_dep_date), '%d %M, %a') AS dep_date,
       DATE_FORMAT(DATE(f_arr_date), '%d %M, %a') AS arr_date,
       base_price AS eco_price
    FROM flight
    WHERE DATE(f_dep_date) = DATE('$in_date') AND departure = '$in_dep' AND arrival = '$in_arr'



