from flask import Blueprint, render_template, request, current_app, session, redirect, url_for
from sql_provider import SQLProvider
from db_context_manager import DBContextManager
from access import booking_required, payment_required
from db_work import select_dict
from datetime import date
import os
import ast

blueprint_booking = Blueprint('bp_booking', __name__, template_folder='templates')

provider = SQLProvider(os.path.join(os.path.dirname(__file__), 'sql'))


@blueprint_booking.route('/', methods=['GET', 'POST'])
def booking():
    if request.method == 'GET':
        return render_template('booking_list.html', min=date.today())
    elif request.form.get('eco') is None and request.form.get('bus') is None:
        dbconfig = current_app.config['dbconfig']
        in_dep, in_arr, in_date = request.form['in_dep'], request.form['in_arr'], request.form['in_date']
        info_sql = provider.get('ticket_info.sql', in_date=in_date, in_dep=in_dep, in_arr=in_arr)
        items = select_dict(dbconfig, info_sql)
        if items:
            for item in items:
                bonus_sql = provider.get('ticket_bonus.sql', in_price=item['eco_price'])
                item['eco_bonus'] = select_dict(dbconfig, bonus_sql)[0]['bonus']
                item['bus_price'] = item['eco_price'] * 2
                bonus_sql = provider.get('ticket_bonus.sql', in_price=item['bus_price'])
                item['bus_bonus'] = select_dict(dbconfig, bonus_sql)[0]['bonus']
            return render_template('booking_list.html', items=items, min=date.today(), dep=in_dep, arr=in_arr,
                                   date=in_date)
        else:
            return render_template('booking_list.html', error='Рейсы не найдены', min=date.today(), dep=in_dep,
                                   arr=in_arr, date=in_date)
    else:
        cur_booking = ast.literal_eval(request.form.get('item'))
        if request.form.get('bus'):
            cur_booking['ticket_type'] = 'bus'
        else:
            cur_booking['ticket_type'] = 'eco'
        session['booking'] = cur_booking
        session.permanent = True
        return redirect(url_for('bp_booking.payment'))


@blueprint_booking.route('/pay', methods=['GET', 'POST'])
@booking_required
def payment():
    cur_booking = session.get('booking', {})
    if cur_booking['ticket_type'] == 'eco':
        cur_booking['price'] = cur_booking['eco_price']
        cur_booking['f_bonus'] = cur_booking['eco_bonus']
        cur_booking['ticket_type'] = 'Эконом'
    else:
        cur_booking['price'] = cur_booking['bus_price']
        cur_booking['f_bonus'] = cur_booking['bus_bonus']
        cur_booking['ticket_type'] = 'Бизнес'
    limit1 = cur_booking['price']
    limit2 = int(limit1 / 2)
    if request.method == 'GET':
        dbconfig = current_app.config['dbconfig']
        user_id = session.get('user_id')
        p_id_sql = provider.get('p_id.sql', input_id=user_id)
        p_id = select_dict(dbconfig, p_id_sql)[0]['p_id']
        balance_sql = provider.get('balance.sql', input_id=p_id)
        res = select_dict(dbconfig, balance_sql)[0]
        if res['balance'] is None:
            res['balance'] = 0
        cur_booking['balance'], cur_booking['coef'], cur_booking['p_id'] = res['balance'], res['coefficient'], p_id
        return render_template('booking_payment.html', item=cur_booking, limit1=limit1, limit2=limit2)
    else:
        if request.form.get('in_bonus'):
            if int(request.form.get('in_bonus')) <= int(cur_booking['balance']):
                cur_booking['in_bonus2'] = request.form.get('in_bonus')
            else:
                return render_template('booking_payment.html', item=cur_booking, limit1=limit1, limit2=limit2,
                                       error='На вашем счету недостаточно миль',)
        else:
            cur_booking['in_bonus2'] = 0
        cur_booking['in_bonus1'] = int(int(cur_booking['in_bonus2']) / 2)
        session['booking'] = cur_booking
        session.permanent = True
        return redirect(url_for('bp_booking.success'))


@blueprint_booking.route('/pay/success')
@booking_required
@payment_required
def success():
    cur_booking = session.get('booking', {})
    cur_booking['overall'] = int(int(cur_booking['f_bonus']) * float(cur_booking['coef']))
    cur_booking['upd_bonus'] = cur_booking['overall'] - int(cur_booking['in_bonus2'])
    cur_booking['new_balance'] = int(cur_booking['balance']) + cur_booking['upd_bonus']
    cur_booking['new_price'] = cur_booking['price'] - cur_booking['in_bonus1']
    t_id = create_booking(current_app.config['dbconfig'], cur_booking)
    session.pop('booking')
    if t_id is None:
        return render_template('payment_failure.html')
    return render_template('payment_success.html', item=cur_booking, t_id=t_id)


def create_booking(dbconfig: dict, cur_booking: dict):
    with DBContextManager(dbconfig) as cursor:
        if cursor is None:
            raise ValueError('Курсор не создан')
        ins_sql = provider.get('insert_ticket.sql', in_price=cur_booking['new_price'], in_bonus=cur_booking['overall'],
                               in_wasted=cur_booking['in_bonus2'], in_date=date.today(), in_p_id=cur_booking['p_id'],
                               in_f_id=cur_booking['id'], in_type=cur_booking['ticket_type'])
        id_sql = 'SELECT LAST_INSERT_ID() AS id;'
        status_sql = provider.get('new_status.sql', input_balance=cur_booking['new_balance'])
        result1 = cursor.execute(ins_sql)
        if result1 == 1:
            cursor.execute(id_sql)
            t_id = cursor.fetchall()[0][0]
            cursor.execute(status_sql)
            s_id = cursor.fetchall()[0][0]
            upd_sql = provider.get('update_bonus.sql', input_bonus=cur_booking['new_balance'],
                                   input_id=cur_booking['p_id'], input_status=s_id, input_date=date.today())
            result2 = cursor.execute(upd_sql)
            if result2 == 1:
                return t_id
        return None
