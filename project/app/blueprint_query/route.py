from flask import Blueprint, request, render_template, current_app
from db_work import select
from sql_provider import SQLProvider
import os

blueprint_query = Blueprint('bp_query', __name__, template_folder='templates')

provider = SQLProvider(os.path.join(os.path.dirname(__file__), 'sql'))


@blueprint_query.route('/')
def pattern_selection():
    return render_template('sql_patterns.html')


@blueprint_query.route('/ticket_info', methods=['GET', 'POST'])
def ticket_info():
    if request.method == 'GET':
        return render_template('ticket_info_form.html')
    else:
        date_str = request.form.get('month_num')
        year, month = date_str.split('-')
        _sql = provider.get('ticket_info.sql', year_input=year, month_input=month)
        result, schema = select(current_app.config['dbconfig'], _sql)
        schema = ['ID', 'Цена', 'Списано миль', 'Начислено миль', 'Дата брони', 'ID пассажира', 'ID рейса', 'Класс']
        if result:
            return render_template('result_set.html', schema=schema, result=result)
        else:
            return render_template('ticket_info_form.html', error='Результаты не найдены')


@blueprint_query.route('/ticket_price', methods=['GET', 'POST'])
def ticket_price():
    if request.method == 'GET':
        return render_template('ticket_price_form.html')
    else:
        date_str = request.form.get('month_num')
        year, month = date_str.split('-')
        _sql = provider.get('ticket_price.sql', year_input=year, month_input=month)
        result, schema = select(current_app.config['dbconfig'], _sql)
        schema = ['День месяца', 'Общая стоимость']
        if result:
            return render_template('result_set.html', schema=schema, result=result)
        else:
            return render_template('ticket_price_form.html', error='Результаты не найдены')
