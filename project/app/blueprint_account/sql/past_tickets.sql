SELECT departure AS dep,
       arrival AS arr,
       DATE(f_dep_date) AS date,
       t_id,
       price,
       class,
       wasted,
       bonus_add,
       base_price
    FROM flight JOIN ticket USING (f_id)
    WHERE DATE(f_dep_date) < '$in_date' AND p_id = '$in_id'