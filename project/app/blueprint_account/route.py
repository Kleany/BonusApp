from flask import Blueprint, render_template, request, current_app, session, redirect, url_for
from sql_provider import SQLProvider
from db_work import select_dict
from datetime import date
import os

blueprint_account = Blueprint('bp_account', __name__, template_folder='templates')

provider = SQLProvider(os.path.join(os.path.dirname(__file__), 'sql'))


@blueprint_account.route('/')
def account():
    user_id = session.get('user_id')
    id_sql = provider.get('user_id.sql', input_id=user_id)
    p_id = select_dict(current_app.config['dbconfig'], id_sql)[0]['p_id']
    session['p_id'] = p_id
    info_sql = provider.get('user_info.sql', input_id=p_id)
    result = select_dict(current_app.config['dbconfig'], info_sql)[0]
    if result['email'] is None:
        result['email'] = 'Не указан'
    if result['phone'] is None:
        result['phone'] = 'Не указан'
    if result['p_sex'] is None:
        result['p_sex'] = 'Не указан'
    elif result['p_sex'] == 'М':
        result['p_sex'] = 'Мужской'
    elif result['p_sex'] == 'Ж':
        result['p_sex'] = 'Женский'
    if result['birthday'] is None:
        result['birthday'] = 'Не указана'
    if result['s_name'] == 'Top':
        result['remaining'] = 'Вы достигли максимального уровня лояльности'
    return render_template('profile_menu.html', input=result)


@blueprint_account.route('tickets', methods=['GET', 'POST'])
def tickets():
    if request.method == 'GET':
        return render_template('profile_tickets.html')
    else:
        dbconfig = current_app.config['dbconfig']
        p_id = session.get('p_id')
        if request.form['type'] == '1':
            sql = provider.get('past_tickets.sql', in_date=date.today(), in_id=p_id)
        else:
            sql = provider.get('cur_tickets.sql', in_date=date.today(), in_id=p_id)
        items = select_dict(dbconfig, sql)
        if items:
            for item in items:
                if item['class'] == 'Бизнес':
                    item['base_price'] *= 2
            return render_template('profile_tickets.html', items=items)
        else:
            return render_template('profile_tickets.html', error='Бронирования не найдены')
