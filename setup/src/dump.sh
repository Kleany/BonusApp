#!/bin/bash

# Setting an SSH-tunnel
ssh -N -n -L *:3306:localhost:3306 root@mysql://db

# Connecting to DB and checking for existing structure
status = $(mysql --host=mysql://db --port=3306 --user=root --password=1111 bonus_program < test.sql)

# Importing DB structure if nonexsitent
if [[ $status = 1 ]]
then
  $? = 1
  mysql --host=mysql://db --port=3306 --user=root --password=1111 bonus_program < dump.sql
fi
