-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: bonus_program
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bonus_report`
--

DROP TABLE IF EXISTS `bonus_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bonus_report` (
  `brep_id` int NOT NULL AUTO_INCREMENT,
  `brep_year` int NOT NULL,
  `brep_month` int NOT NULL,
  `brep_amount` int NOT NULL,
  PRIMARY KEY (`brep_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bonus_report`
--

LOCK TABLES `bonus_report` WRITE;
/*!40000 ALTER TABLE `bonus_report` DISABLE KEYS */;
INSERT INTO `bonus_report` VALUES (11,2020,3,4250),(13,2022,2,5400);
/*!40000 ALTER TABLE `bonus_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `external_user`
--

DROP TABLE IF EXISTS `external_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `external_user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `p_id` int NOT NULL,
  `user_group` varchar(45) DEFAULT NULL,
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `external_passanger_idx` (`p_id`),
  CONSTRAINT `external_passenger` FOREIGN KEY (`p_id`) REFERENCES `passenger` (`p_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `external_user`
--

LOCK TABLES `external_user` WRITE;
/*!40000 ALTER TABLE `external_user` DISABLE KEYS */;
INSERT INTO `external_user` VALUES (1,1,NULL,'nikita','nikita'),(2,5,NULL,'artem','artem'),(3,9,NULL,'katya','katya'),(4,4,NULL,'alex','alex'),(5,2,NULL,'yulia','yulia'),(6,3,NULL,'alexandr','alexandr'),(7,6,NULL,'eva','eva'),(8,7,NULL,'kolya','kolya');
/*!40000 ALTER TABLE `external_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight`
--

DROP TABLE IF EXISTS `flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flight` (
  `f_id` int NOT NULL,
  `departure` varchar(45) NOT NULL,
  `arrival` varchar(45) NOT NULL,
  `f_dep_date` datetime NOT NULL,
  `f_dep_time_zone` varchar(8) NOT NULL,
  `f_arr_date` datetime NOT NULL,
  `f_arr_time_zone` varchar(8) NOT NULL,
  `base_price` int NOT NULL,
  PRIMARY KEY (`f_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight`
--

LOCK TABLES `flight` WRITE;
/*!40000 ALTER TABLE `flight` DISABLE KEYS */;
INSERT INTO `flight` VALUES (1,'╨Ш╤А╨║╤Г╤В╤Б╨║','╨Ь╨╛╤Б╨║╨▓╨░','2022-02-08 09:35:00','+08:00','2022-02-08 11:15:00','+03:00',5750),(2,'╨Ь╨╛╤Б╨║╨▓╨░','╨Ш╤А╨║╤Г╤В╤Б╨║','2020-03-08 21:35:00','+03:00','2020-03-09 07:25:00','+08:00',5500),(3,'╨Ш╤А╨║╤Г╤В╤Б╨║','╨Ь╨╛╤Б╨║╨▓╨░','2020-03-04 18:10:00','+08:00','2020-03-04 20:35:00','+03:00',5000),(4,'╨Ь╨╛╤Б╨║╨▓╨░','╨б╨░╨╝╨░╤А╨░','2021-11-29 14:40:00','+03:00','2021-11-29 20:10:00','+04:00',3250),(5,'╨Т╨╗╨░╨┤╨╕╨▓╨╛╤Б╤В╨╛╨║','╨Э╨╛╨▓╨╛╤Б╨╕╨▒╨╕╤А╤Б╨║','2020-03-01 15:10:00','+10:00','2020-03-01 17:55:00','+07:00',4500),(6,'╨Ъ╤А╨░╤Б╨╜╨╛╤П╤А╤Б╨║','╨Ш╤А╨║╤Г╤В╤Б╨║','2022-04-13 12:20:00','+07:00','2022-04-13 14:30:00','+08:00',2500),(7,'╨Ь╨╛╤Б╨║╨▓╨░','╨Ш╤А╨║╤Г╤В╤Б╨║','2022-12-29 07:10:00','+03:00','2022-12-29 18:35:00','+08:00',5000),(8,'╨Ь╨╛╤Б╨║╨▓╨░','╨Ш╤А╨║╤Г╤В╤Б╨║','2022-12-29 21:35:00','+03:00','2022-12-30 08:10:00','+08:00',4900),(9,'╨Ь╨╛╤Б╨║╨▓╨░','╨Ш╤А╨║╤Г╤В╤Б╨║','2022-12-29 23:20:00','+03:00','2022-12-30 10:00:00','+08:00',4700),(10,'╨Ш╤А╨║╤Г╤В╤Б╨║','╨Ь╨╛╤Б╨║╨▓╨░','2023-01-30 09:30:00','+08:00','2023-01-30 11:30:00','+03:00',4320),(11,'╨Ь╨╛╤Б╨║╨▓╨░','╨Ш╤А╨║╤Г╤В╤Б╨║','2023-07-20 21:35:00','+03:00','2023-07-21 07:25:00','+05:00',5000),(12,'╨Ь╨╛╤Б╨║╨▓╨░','╨Ш╤А╨║╤Г╤В╤Б╨║','2023-07-20 23:30:00','+03:00','2023-07-21 09:20:00','+05:00',5100);
/*!40000 ALTER TABLE `flight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history_stmt`
--

DROP TABLE IF EXISTS `history_stmt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `history_stmt` (
  `h_id` int NOT NULL AUTO_INCREMENT,
  `h_date` datetime NOT NULL,
  `p_id` int DEFAULT NULL,
  `f_id` int DEFAULT NULL,
  `h_amount` int NOT NULL,
  PRIMARY KEY (`h_id`),
  KEY `history_passenger_idx` (`p_id`),
  KEY `history_flight_idx` (`f_id`),
  CONSTRAINT `history_flight` FOREIGN KEY (`f_id`) REFERENCES `flight` (`f_id`),
  CONSTRAINT `history_passenger` FOREIGN KEY (`p_id`) REFERENCES `passenger` (`p_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history_stmt`
--

LOCK TABLES `history_stmt` WRITE;
/*!40000 ALTER TABLE `history_stmt` DISABLE KEYS */;
INSERT INTO `history_stmt` VALUES (1,'2022-02-08 09:35:00',1,1,1800),(2,'2022-02-08 09:35:00',5,1,1800),(3,'2020-03-08 21:35:00',3,2,5000),(4,'2020-03-08 21:35:00',6,2,5750),(5,'2020-03-04 18:10:00',2,3,1375),(6,'2020-03-04 18:10:00',4,3,6000),(7,'2021-11-29 14:40:00',7,4,2200),(8,'2020-03-04 19:10:00',5,6,600),(9,'2020-03-01 15:10:00',4,5,1200),(14,'2022-04-01 00:00:00',4,6,600),(38,'2022-12-07 00:00:00',1,8,4200),(39,'2022-12-30 00:00:00',1,10,3600),(40,'2023-01-28 00:00:00',1,10,3750),(41,'2023-06-07 00:00:00',1,11,6000);
/*!40000 ALTER TABLE `history_stmt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `internal_user`
--

DROP TABLE IF EXISTS `internal_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `internal_user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_group` varchar(45) NOT NULL,
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `internal_user`
--

LOCK TABLES `internal_user` WRITE;
/*!40000 ALTER TABLE `internal_user` DISABLE KEYS */;
INSERT INTO `internal_user` VALUES (1,'analyst','analyst','analyst'),(2,'director','director','director');
/*!40000 ALTER TABLE `internal_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passenger`
--

DROP TABLE IF EXISTS `passenger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `passenger` (
  `p_id` int NOT NULL AUTO_INCREMENT,
  `p_name` varchar(45) NOT NULL,
  `p_surname` varchar(45) NOT NULL,
  `p_sex` varchar(1) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `balance` int NOT NULL,
  `p_date` date DEFAULT NULL,
  `s_id` int DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`p_id`),
  KEY `passenger_status_idx` (`s_id`),
  CONSTRAINT `passenger_status` FOREIGN KEY (`s_id`) REFERENCES `status` (`s_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passenger`
--

LOCK TABLES `passenger` WRITE;
/*!40000 ALTER TABLE `passenger` DISABLE KEYS */;
INSERT INTO `passenger` VALUES (1,'╨Э╨╕╨║╨╕╤В╨░','╨Ь╨░╨║╨░╤А╨╡╨╜╨║╨╛','╨Ь','2002-05-13',14410,'2023-06-07',4,'maknik1305@gmail.com','89500774515'),(2,'╨о╨╗╨╕╤П','╨Ш╨▓╨░╨╜╨╛╨▓╨░','╨Ц','1998-03-02',5275,'2020-03-04',2,NULL,NULL),(3,'╨Р╨╗╨╡╨║╤Б╨░╨╜╨┤╤А','╨б╨╕╨┤╨╛╤А╨╛╨▓','╨Ь','1965-08-27',6000,'2020-03-08',2,NULL,NULL),(4,'╨Р╨╗╨╡╨║╤Б╨╡╨╣','╨Ъ╤Г╨╖╨╜╨╡╤Ж╨╛╨▓','╨Ь','2003-01-01',10700,'2022-11-30',3,NULL,NULL),(5,'╨Р╤А╤В╨╡╨╝','╨б╤В╨░╤А╨╕╨║╨╛╨▓','╨Ь','2002-11-28',18100,'2022-02-08',5,NULL,NULL),(6,'╨н╨▓╨╡╨╗╨╕╨╜╨░','╨Р╨║╨╛╨┐╤П╨╜','╨Ц','1975-04-26',8750,'2020-03-08',3,NULL,NULL),(7,'╨Э╨╕╨║╨╛╨╗╨░╨╣','╨п╤Ж╨╡╨╜╨║╨╛','╨Ь','1983-07-10',4000,'2021-11-29',2,NULL,NULL),(9,'╨Х╨║╨░╤В╨╡╤А╨╕╨╜╨░','╨Я╨╡╤В╤А╨╛╨▓╨░',NULL,NULL,0,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `passenger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scale`
--

DROP TABLE IF EXISTS `scale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scale` (
  `sc_id` int NOT NULL AUTO_INCREMENT,
  `min` float NOT NULL,
  `max` float NOT NULL,
  `amount` int NOT NULL,
  PRIMARY KEY (`sc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scale`
--

LOCK TABLES `scale` WRITE;
/*!40000 ALTER TABLE `scale` DISABLE KEYS */;
INSERT INTO `scale` VALUES (1,1000,1499,125),(2,1500,1999,250),(3,2000,2499,375),(4,2500,2999,500),(5,3000,3499,625),(6,3500,3999,750),(7,4000,4499,875),(8,4500,4999,1000),(9,5000,5499,1250),(10,5500,5999,1500),(11,6000,6499,1750),(12,6500,6999,2000),(13,7000,7499,2250),(14,7500,7999,2500),(15,8000,8499,2750),(16,8500,8999,3000),(17,9000,9499,3250),(18,9500,9999,3500),(19,10000,12000,5000);
/*!40000 ALTER TABLE `scale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status` (
  `s_id` int NOT NULL AUTO_INCREMENT,
  `benefits` varchar(45) DEFAULT NULL,
  `coefficient` float NOT NULL,
  `upgrade_min` int NOT NULL,
  `upgrade_max` int NOT NULL,
  `s_name` varchar(45) NOT NULL,
  PRIMARY KEY (`s_id`),
  UNIQUE KEY `upgrade_min_UNIQUE` (`upgrade_min`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'',1,0,3999,'Classic'),(2,'╨Ь╨╡╤Б╤В╨╛',1.1,4000,7999,'Junior'),(3,'╨Ь╨╡╤Б╤В╨╛, ╨▒╨░╨│╨░╨╢',1.15,8000,11999,'Master'),(4,'╨Ь╨╡╤Б╤В╨╛, ╨▒╨░╨│╨░╨╢, ╨╛╨▓╨╡╤А╨┤╤А╨░╤Д╤В',1.2,12000,15999,'Expert'),(5,'╨Ь╨╡╤Б╤В╨╛, ╨▒╨░╨│╨░╨╢, ╨╛╨▓╨╡╤А╨┤╤А╨░╤Д╤В, ╨▒╨╕╨╖╨╜╨╡╤Б',1.25,16000,200000,'Top');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket` (
  `t_id` int NOT NULL AUTO_INCREMENT,
  `price` int NOT NULL,
  `wasted` int NOT NULL,
  `bonus_add` int NOT NULL,
  `t_date` date NOT NULL,
  `p_id` int DEFAULT NULL,
  `f_id` int DEFAULT NULL,
  `class` varchar(45) NOT NULL,
  PRIMARY KEY (`t_id`),
  KEY `ticket-passenger_idx` (`p_id`),
  KEY `ticket_flight_idx` (`f_id`),
  CONSTRAINT `ticket_flight` FOREIGN KEY (`f_id`) REFERENCES `flight` (`f_id`),
  CONSTRAINT `ticket_passenger` FOREIGN KEY (`p_id`) REFERENCES `passenger` (`p_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` VALUES (1,5750,0,1800,'2022-01-23',1,1,'╨н╨║╨╛╨╜╨╛╨╝'),(2,5750,0,1800,'2022-01-22',5,1,'╨н╨║╨╛╨╜╨╛╨╝'),(3,11000,0,5000,'2020-03-01',3,2,'╨н╨║╨╛╨╜╨╛╨╝'),(4,11000,0,5750,'2020-03-03',6,2,'╨С╨╕╨╖╨╜╨╡╤Б'),(5,5000,0,1375,'2020-03-01',2,3,'╨н╨║╨╛╨╜╨╛╨╝'),(6,10000,0,6000,'2020-03-02',4,3,'╨С╨╕╨╖╨╜╨╡╤Б'),(7,6500,0,2200,'2021-11-09',7,4,'╨С╨╕╨╖╨╜╨╡╤Б'),(8,4500,0,1200,'2020-03-01',4,5,'╨н╨║╨╛╨╜╨╛╨╝'),(9,2500,0,600,'2022-04-01',5,6,'╨н╨║╨╛╨╜╨╛╨╝'),(10,2500,0,600,'2022-04-01',4,6,'╨н╨║╨╛╨╜╨╛╨╝'),(60,8800,2000,4200,'2022-12-07',1,8,'╨С╨╕╨╖╨╜╨╡╤Б'),(61,8640,0,3600,'2022-12-30',1,10,'╨С╨╕╨╖╨╜╨╡╤Б'),(62,4320,8640,3750,'2023-01-28',1,10,'╨С╨╕╨╖╨╜╨╡╤Б'),(63,7500,5000,6000,'2023-06-07',1,11,'╨С╨╕╨╖╨╜╨╡╤Б');
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trigger_history_insert` AFTER INSERT ON `ticket` FOR EACH ROW BEGIN
    INSERT history_stmt
		VALUES (NULL, new.t_date, new.p_id, new.f_id, new.bonus_add);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary view structure for view `ticket_amount_2020_03`
--

DROP TABLE IF EXISTS `ticket_amount_2020_03`;
/*!50001 DROP VIEW IF EXISTS `ticket_amount_2020_03`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `ticket_amount_2020_03` AS SELECT 
 1 AS `p_id`,
 1 AS `t_amount`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ticket_report`
--

DROP TABLE IF EXISTS `ticket_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket_report` (
  `trep_id` int NOT NULL AUTO_INCREMENT,
  `trep_year` int NOT NULL,
  `trep_month` int NOT NULL,
  `trep_amount` int NOT NULL,
  PRIMARY KEY (`trep_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_report`
--

LOCK TABLES `ticket_report` WRITE;
/*!40000 ALTER TABLE `ticket_report` DISABLE KEYS */;
INSERT INTO `ticket_report` VALUES (6,2022,1,17250);
/*!40000 ALTER TABLE `ticket_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `ticket_amount_2020_03`
--

/*!50001 DROP VIEW IF EXISTS `ticket_amount_2020_03`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ticket_amount_2020_03` (`p_id`,`t_amount`) AS select `ticket`.`p_id` AS `p_id`,count(`ticket`.`t_id`) AS `COUNT(t_id)` from `ticket` where ((year(`ticket`.`t_date`) = 2020) and (month(`ticket`.`t_date`) = 3)) group by `ticket`.`p_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-14 12:07:07
